# -*- coding: UTF-8 -*-
from django.db import models

# Create your models here.

GENDER = (
    ('M', 'Ч'),
    ('F', 'Ж')
)

VISITING = (
    ('first', 'First time'),
    ('repeated', 'Repeated')
)


class Payment_settings(models.Model):
    price = models.FloatField(unique=True, verbose_name=u'Ціна')
    visit_type = models.CharField(choices=VISITING, max_length=10, verbose_name=u'Тип послуг')

    class Meta:
        verbose_name = u'Налаштування'
        verbose_name_plural = u'Налаштування'

    def __unicode__(self):
        return u'%s %s' % (self.visit_type, str(self.price))

class User(models.Model):
    first_name = models.CharField(max_length=128, verbose_name=u'Ім\'я')
    second_name = models.CharField(max_length=128, verbose_name=u'По батькові')
    last_name = models.CharField(max_length=128, verbose_name=u'Прізвище')
    birthday = models.DateField(verbose_name=u'Дата народження')
    gender = models.CharField(choices=GENDER, max_length=10, verbose_name=u'Стать')
    address = models.CharField(max_length=255, verbose_name=u'Адреса')
    phone = models.CharField(max_length=255, verbose_name=u'Телефон', null=True, blank=True)

    class Meta:
        verbose_name = u'Пацієнти'
        verbose_name_plural = u'Пацієнти'

    def __unicode__(self):
        return u'%s %s %s' % (self.last_name, self.first_name, self.second_name)



class History(models.Model):
    patient = models.ForeignKey(User, null=True, on_delete=models.SET_NULL,
                                verbose_name=u'Пацієнт')
    date = models.DateField(auto_now=True, verbose_name=u'Дата')
    # visiting = models.CharField(choices=VISITING, max_length=10, verbose_name='Повторний сеанс')
    diagnosis = models.TextField(null=True, verbose_name=u'Діагноз')
    additional_information = models.TextField(
        blank=True,
        null=True,
        verbose_name=u'Додаткова інформація')
    therapy = models.TextField(null=True, verbose_name=u'Лікування')
    price = models.IntegerField(verbose_name=u'Ціна')
    repeated = models.BooleanField(default=False, verbose_name=u'Повторне відвідування')
    doctor = models.ForeignKey('Doctor', verbose_name=u'Лікар', blank=False, null=True)

    class Meta:
        verbose_name_plural = u'Історія відвідувань'
        ordering = ['-date']

    def __unicode__(self):
        return u'%s: %s' % (str(self.date.strftime('%x')), self.diagnosis)


class Doctor(models.Model):
    initials = models.CharField(max_length=255, blank=False, verbose_name=u'П.І.Б')
    position = models.CharField(max_length=255, blank=False, verbose_name=u'Посада')

    class Meta:
        verbose_name_plural = u'Лікарі'
        verbose_name = u'Лікар'

    def __unicode__(self):
        return u'%s (%s)' % (self.initials, self.position)


