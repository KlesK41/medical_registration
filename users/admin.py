# -*- coding: UTF-8 -*-
from django.contrib import admin
from .models import *
from datetimewidget.widgets import DateWidget
from django.forms import widgets

admin.site.register(Doctor)

#
# Register your models here.

class HistoryInline(admin.StackedInline):
    model = History
    extra = 0
    readonly_fields = ('date',)
    fieldsets = (
        (u'Запис', {
            'fields': ('date','diagnosis', 'therapy', 'additional_information', 'repeated', 'price'),
            'classes': ('collapse',)
        }),
    )
    verbose_name = u'Запис'



class UserAdmin(admin.ModelAdmin):

    inlines = [
        HistoryInline,
    ]

    list_display_links = ('first_name', 'last_name', 'second_name')
    fields = ('last_name', 'first_name', 'second_name', 'address', 'phone', 'gender', 'birthday')
    list_display = ('id', 'last_name', 'first_name', 'second_name', 'address', 'birthday')
    search_fields = ['last_name', 'first_name', 'second_name']
    list_select_related = True

    formfield_overrides = {
        models.DateField: {
            'widget': DateWidget(
                bootstrap_version=3,
                usel10n=True
            ),
        },
    }

    class Media:
        css = {
            "all": ("css/bootstrap.min.css", "css/admin.custom.css")
        }
        js = ("js/jquery-3.1.1.min.js", "js/bootstrap.min.js",)


admin.site.register(User, UserAdmin)

class HistoryAdmin(admin.ModelAdmin):
    list_display = ('patient', 'date', 'doctor', 'price', 'repeated')
    search_fields = ['patient__first_name', 'patient__last_name', 'patient__second_name', 'date']
    date_hierarchy = 'date'
    list_filter = ('date', 'doctor')

admin.site.register(History, HistoryAdmin)
# admin.site.register(Payment_settings)


admin.autodiscover()

from django.contrib.auth.models import User as Auth
from django.contrib.auth.models import Group

admin.site.unregister(Auth)
admin.site.unregister(Group)

