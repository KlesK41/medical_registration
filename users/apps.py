# -*- coding: UTF-8 -*-
from django.apps import AppConfig


class UsersConfig(AppConfig):
    name = 'users'
    verbose_name = u'Журнал пацієнтів'
