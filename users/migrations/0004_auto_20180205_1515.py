# -*- coding: utf-8 -*-
# Generated by Django 1.10.4 on 2018-02-05 13:15
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0003_auto_20170110_2034'),
    ]

    operations = [
        migrations.CreateModel(
            name='Doctor',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('initials', models.CharField(max_length=255, verbose_name='П.І.Б')),
                ('position', models.CharField(max_length=255, verbose_name='Посада')),
            ],
            options={
                'verbose_name': 'Лікар',
                'verbose_name_plural': 'Лікарі',
            },
        ),
        migrations.AlterField(
            model_name='user',
            name='phone',
            field=models.CharField(blank=True, max_length=255, null=True, verbose_name='Телефон'),
        ),
        migrations.AddField(
            model_name='history',
            name='doctor',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='users.Doctor', verbose_name='Лікар'),
        ),
    ]
